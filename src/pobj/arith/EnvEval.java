/**
 * @author tb (tahirou_bah)
 * 
 */
package pobj.arith;

public class EnvEval {
	/*
	 * le tableau variables[] defini la liste des variables à evaluer
	 */
	private double[] variables;
	public EnvEval(int n){
		this.variables=new double[n];
		
	}
	/**
	 * 
	 * @param i definit l'index de la variable dans le tableau
	 * @param d donne la nouvelle valeur de la position i du tableau
	 */
 public void setVariables(int i, double d){
	 this.variables[i]=d;
	 
 }
 /**
  * @param index la position de l'element à revoyer
  * @return return la valeur de l'element qui se trouve la position index
  */
 public double getValue(int index){
	 return this.variables[index];
 }
 @Override
 /**
  * @return 
  */
 public String toString(){
	  String ch="";
	 for(int j=0;j<variables.length;j++)
		 ch+="X"+j+"\n";
	 return ch;
 }
}