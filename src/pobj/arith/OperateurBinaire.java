/**
 * @author tb tahirou_bah
 * cette classe qui implemente l'interface Expression permet de definir une expression
 * left defnit l'operande de gauche
 * right pour l'operande de droite une exemple d'expression arth. (X0 + X1)+0.25
 */
package pobj.arith;

public class OperateurBinaire implements Expression {
	private Expression left;
	private Expression right;
	private Operator type;
	public Expression getLeft() {
		return left;
	}
	public void setLeft(Expression left) {
		this.left = left;
	}
	public Expression getRight() {
		return right;
	}
	public void setRight(Expression right) {
		this.right = right;
	}
	protected OperateurBinaire(Expression left, Expression right, Operator op) {
		//super();
		this.left = left;
		this.right = right;
		this.type=op;
	}
	/**
	 * cette operation est declaré de l'interface Expression
	 * elle permet d'evaluer une expression 
	 * exemple (1+2)-X0=12
	 * envEval definit l'environnement d'evaluation
	 */
	@Override
	public double eval(EnvEval envEval) {
		
		double resultat=0;
		switch (this.type) {
		case PLUS:
			resultat= this.getLeft().eval(envEval)+this.getRight().eval(envEval);
			break;
		case DIV:
			/**
			 * ce bloc try-catch permet de lever l'excption division par zero
			 */
			try{
			resultat=this.getLeft().eval(envEval)/this.getRight().eval(envEval);
			}catch (ArithmeticException e) {
				System.out.println("Division par zero");
				break;
			}
			break;
		case MULT:
			resultat=this.getLeft().eval(envEval)*this.getRight().eval(envEval);
			break;
		case MINUS:
			resultat=this.getLeft().eval(envEval)-this.getRight().eval(envEval);
			break;
		}		
		return resultat;
	}
	@Override
	public String toString(){
		String ch="";
		switch (this.type) {
		case DIV: ch=this.getLeft()+"/"+this.getRight(); break;
		case MULT:
			ch=this.getLeft()+"*"+this.getRight(); break;
		case PLUS:
			ch+=this.getLeft()+"+"+this.getRight(); break;

		case MINUS:
			ch=this.getLeft()+"-"+this.getRight(); break;
		}
		return ch;
		
	}

}
