package pobj.algogen;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
public class Population {
	private static final int POP_SIZE =20;
	private List<Individu> individus ;
	//private int size =0;
	public Population(){
		individus =new ArrayList<Individu>();
	}
	public int  size () {
		return this.individus.size();
	}

	protected void add (Individu individu){
		if(individus.size()<POP_SIZE){
		    	individus.add(individu);
			
		}else{


			throw new ArrayIndexOutOfBoundsException ("Plus de place");
		}
}
	@Override
	public String toString (){
		String ch=""; int i=0;
		for(Individu id:individus){
			ch += id.toString()+"\n";
			i++;
			System.out.println("la position est: "+i);}
		return ch;
	}
	/**
	 * @param env cibled environnement
	 */
	public void evaluer(Environnement env){
		for(Individu id:individus)
			env.eval(id);
		Collections.sort(individus);
	}
	public Population evoluer (Environnement env){
		Population pop =new Population();
		this.evaluer(env);
		for(int i=0;i<this.size()/5;i++)
			pop.add(individus.get(i).clone());
				return pop;			
	}
	/**
	 * 
	 */
	public Population reproduire(Environnement  env){
		Population pop= this.evoluer(env);
		Random generator=new Random();
		
		int indx1=generator.nextInt(this.size()/5);
		int indx2=generator.nextInt(this.size()/5);
		for(int i=this.size()/5;i<this.size();i++){
			Individu ind1=pop.individus.get(indx1);
			Individu ind2=pop.individus.get(indx2);
			ind1.muter();
			ind2.muter();
			ind1.croiser(ind2);
			pop.add(ind1);
		}
		System.out.println("la nouvelle taille est:: "+pop.size());
		return pop;
		
	}
}
